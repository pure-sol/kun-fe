export class Student {
  id: number;
  first_name: string;
  last_name: string;
  date_of_birth: Date;
  class: {}

  constructor(){
    this.class = {
      id: null
    };
  }
}
