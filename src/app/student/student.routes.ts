import { Routes } from '@angular/router';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentEditComponent } from './student-edit/student-edit.component';

export const STUDENT_ROUTES: Routes = [
  {
    path: 'students',
    component: StudentListComponent
  },
  {
    path: 'students/class/:id',
    component: StudentListComponent
  },
  {
    path: 'students/:id',
    component: StudentEditComponent
  }
];
