import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { StudentService } from '../student.service';
import { Student } from '../student';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { KunclassService } from 'src/app/kunclass/kunclass.service';
import { Kunclass } from 'src/app/kunclass/kunclass';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html'
})
export class StudentEditComponent implements OnInit {

  id: string;
  student: Student;
  feedback: any = {};
  classes: Array<Kunclass>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studentService: StudentService,
    private kunclassService: KunclassService
    ) {
  }

  ngOnInit() {
    this.kunclassService.getClasses().subscribe(result => {
      this.classes = result.data;
    });
    
    this
      .route
      .params
      .pipe(
        map(p => p.id),
        switchMap(id => {
          if (id === 'new') { return of(new Student()); }
          return this.studentService.findById(id);
        })
      )
      .subscribe(student => {
          this.student = student.data || new Student();
          this.feedback = {};
        },
        err => {
          this.feedback = {type: 'warning', message: 'Error loading'};
        }
      );
  }

  save() {
    this.student.class_id = this.student.class.id;
    this.studentService.save(this.student).subscribe(
      student => {
        this.student = student;
        this.feedback = {type: 'success', message: 'Save was successful!'};
        setTimeout(() => {
          this.router.navigate(['/students']);
        }, 1000);
      },
      err => {
        this.feedback = {type: 'warning', message: 'Error saving'};
      }
    );
  }

  cancel() {
    this.router.navigate(['/students']);
  }
}
