import { Component, OnInit } from '@angular/core';
import { StudentFilter } from '../student-filter';
import { StudentService } from '../student.service';
import { Student } from '../student';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-student',
  templateUrl: 'student-list.component.html'
})
export class StudentListComponent implements OnInit {

  filter = new StudentFilter();
  selectedStudent: Student;
  feedback: any = {};
  route: ActivatedRoute;

  get studentList(): Student[] {
    return this.studentService.studentList;
  }

  constructor(private studentService: StudentService) {
  }

  ngOnInit() {
    this.search();
  }

  search(): void {
    this.studentService.load(this.filter);
  }

  select(selected: Student): void {
    this.selectedStudent = selected;
  }

  delete(student: Student): void {
    if (confirm('Are you sure?')) {
      this.studentService.delete(student).subscribe(() => {
          this.feedback = {type: 'success', message: 'Delete was successful!'};
          setTimeout(() => {
            this.search();
          }, 1000);
        },
        err => {
          this.feedback = {type: 'warning', message: 'Error deleting.'};
        }
      );
    }
  }
}
