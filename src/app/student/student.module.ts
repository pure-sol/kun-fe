import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentEditComponent } from './student-edit/student-edit.component';
import { StudentService } from './student.service';
import { STUDENT_ROUTES } from './student.routes';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(STUDENT_ROUTES)
  ],
  declarations: [
    StudentListComponent,
    StudentEditComponent
  ],
  providers: [StudentService],
  exports: []
})
export class StudentModule { }
