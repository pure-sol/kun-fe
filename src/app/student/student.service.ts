// @ts-nocheck
import { Student } from './student';
import { StudentFilter } from './student-filter';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const headers = new HttpHeaders().set('Accept', 'application/json');

@Injectable()
export class StudentService {
  studentList: Student[] = [];
  api = 'http://kun.loc/api/students';

  constructor(private http: HttpClient) {
  }

  findById(id: string): Observable<Student> {
    const url = `${this.api}/${id}`;
    const params = { id: id };
    return this.http.get<Student>(url, {params, headers});
  }

  load(filter: StudentFilter): void {
    this.find(filter).subscribe(result => {
        this.studentList = result.data;
      },
      err => {
        console.error('error loading', err);
      }
    );
  }

  find(filter: StudentFilter): Observable<Student[]> {
    const params = {
    };

    return this.http.get<Student[]>(this.api, {params, headers});
  }

  save(entity: Student): Observable<Student> {
    let params = new HttpParams();
    let url = '';
    if (entity.id) {
      url = `${this.api}/${entity.id.toString()}`;
      params = new HttpParams().set('ID', entity.id.toString());
      return this.http.put<Student>(url, entity, {headers, params});
    } else {
      url = `${this.api}`;
      return this.http.post<Student>(url, entity, {headers, params});
    }
  }

  delete(entity: Student): Observable<Student> {
    let params = new HttpParams();
    let url = '';
    if (entity.id) {
      url = `${this.api}/${entity.id.toString()}`;
      params = new HttpParams().set('ID', entity.id.toString());
      return this.http.delete<Student>(url, {headers, params});
    }
    return null;
  }
}

