// @ts-nocheck
import { Kunclass } from './kunclass';
import { KunclassFilter } from './kunclass-filter';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const headers = new HttpHeaders().set('Accept', 'application/json');

@Injectable()
export class KunclassService {
  kunclassList = [];
  api = 'http://kun.loc/api/classes';

  constructor(private http: HttpClient) {
  }

  findById(id: string): Observable<Kunclass> {
    const url = `${this.api}/${id}`;
    const params = { id: id };
    return this.http.get<any>(url, {params, headers});
  }

  load(filter: KunclassFilter): void {
    this.find(filter).subscribe(result => {
        this.kunclassList = result.data;
      },
      err => {
        console.error('error loading', err);
      }
    );
  }

  getClasses() {
    return this.http.get<any[]>(this.api);
  }

  find(filter: KunclassFilter): Observable<Kunclass[]> {
    const params = {
      'code': filter.code,
      'name': filter.name,
    };

    return this.http.get<any[]>(this.api, {params, headers});
  }

  save(entity: Kunclass): Observable<Kunclass> {
    let params = new HttpParams();
    let url = '';
    if (entity.id) {
      url = `${this.api}/${entity.id.toString()}`;
      // params = new HttpParams().set('ID', entity.id.toString());
      return this.http.put<Kunclass>(url, entity, {headers, params});
    } else {
      url = `${this.api}`;
      return this.http.post<Kunclass>(url, entity, {headers, params});
    }
  }

  delete(entity: Kunclass): Observable<Kunclass> {
    let params = new HttpParams();
    let url = '';
    if (entity.id) {
      url = `${this.api}/${entity.id.toString()}`;
      // params = new HttpParams().set('ID', entity.id.toString());
      return this.http.delete<Kunclass>(url, {headers, params});
    }
    return null;
  }
}

