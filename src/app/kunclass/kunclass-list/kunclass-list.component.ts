import { Component, OnInit } from '@angular/core';
import { KunclassFilter } from '../kunclass-filter';
import { KunclassService } from '../kunclass.service';
import { Kunclass } from '../kunclass';

@Component({
  selector: 'app-kunclass',
  templateUrl: 'kunclass-list.component.html'
})
export class KunclassListComponent implements OnInit {

  filter = new KunclassFilter();
  selectedKunclass: Kunclass;
  feedback: any = {};

  get kunclassList(): Kunclass[] {
    return this.kunclassService.kunclassList;
  }

  constructor(private kunclassService: KunclassService) {
  }

  ngOnInit() {
    this.search();
  }

  search(): void {
    this.kunclassService.load(this.filter);
  }

  select(selected: Kunclass): void {
    this.selectedKunclass = selected;
  }

  delete(kunclass: Kunclass): void {
    if (confirm('Are you sure?')) {
      this.kunclassService.delete(kunclass).subscribe(() => {
          this.feedback = {type: 'success', message: 'Delete was successful!'};
          setTimeout(() => {
            this.search();
          }, 1000);
        },
        err => {
          this.feedback = {type: 'warning', message: 'Error deleting.'};
        }
      );
    }
  }
}
