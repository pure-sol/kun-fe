import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { KunclassListComponent } from './kunclass-list.component';
import { KunclassService } from '../kunclass.service';

describe('KunclassListComponent', () => {
  let component: KunclassListComponent;
  let fixture: ComponentFixture<KunclassListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KunclassListComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule],
      providers: [KunclassService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KunclassListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
