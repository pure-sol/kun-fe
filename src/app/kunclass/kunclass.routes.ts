import { Routes } from '@angular/router';
import { KunclassListComponent } from './kunclass-list/kunclass-list.component';
import { KunclassEditComponent } from './kunclass-edit/kunclass-edit.component';

export const KUNCLASS_ROUTES: Routes = [
  {
    path: 'kunclasses',
    component: KunclassListComponent
  },
  {
    path: 'kunclasses/:id',
    component: KunclassEditComponent
  }
];
