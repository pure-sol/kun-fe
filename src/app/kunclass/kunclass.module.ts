import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { KunclassListComponent } from './kunclass-list/kunclass-list.component';
import { KunclassEditComponent } from './kunclass-edit/kunclass-edit.component';
import { KunclassService } from './kunclass.service';
import { KUNCLASS_ROUTES } from './kunclass.routes';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(KUNCLASS_ROUTES)
  ],
  declarations: [
    KunclassListComponent,
    KunclassEditComponent
  ],
  providers: [KunclassService],
  exports: []
})
export class KunclassModule { }
