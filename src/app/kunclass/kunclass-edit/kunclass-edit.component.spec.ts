import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { KunclassEditComponent } from './kunclass-edit.component';
import { KunclassService } from '../kunclass.service';

describe('KunclassEditComponent', () => {
  let component: KunclassEditComponent;
  let fixture: ComponentFixture<KunclassEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KunclassEditComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule],
      providers: [KunclassService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KunclassEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
