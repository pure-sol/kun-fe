import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { KunclassService } from '../kunclass.service';
import { Kunclass } from '../kunclass';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-kunclass-edit',
  templateUrl: './kunclass-edit.component.html'
})
export class KunclassEditComponent implements OnInit {

  id: string;
  kunclass: Kunclass;
  feedback: any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private kunclassService: KunclassService) {
  }

  ngOnInit() {
    this
      .route
      .params
      .pipe(
        map(p => p.id),
        switchMap(id => {
          if (id === 'new') { return of(new Kunclass()); }
          return this.kunclassService.findById(id);
        })
      )
      .subscribe(kunclass => {
          this.kunclass = kunclass.data || kunclass;
          this.feedback = {};
        },
        err => {
          this.feedback = {type: 'warning', message: 'Error loading'};
        }
      );
  }

  save() {
    this.kunclassService.save(this.kunclass).subscribe(
      kunclass => {
        this.kunclass = kunclass;
        this.feedback = {type: 'success', message: 'Save was successful!'};
        setTimeout(() => {
          this.router.navigate(['/kunclasses']);
        }, 1000);
      },
      err => {
        this.feedback = {type: 'warning', message: 'Error saving'};
      }
    );
  }

  cancel() {
    this.router.navigate(['/kunclasses']);
  }
}
