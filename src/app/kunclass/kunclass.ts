export class Kunclass {
  id: number;
  code: string;
  name: string;
  maximum_students: number;
  status: string;
  description: string;
}
