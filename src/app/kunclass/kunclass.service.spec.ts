import { TestBed } from '@angular/core/testing';
import { KunclassService } from './kunclass.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('KunclassService', () => {
  let service: KunclassService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [KunclassService]
    });

    service = TestBed.get(KunclassService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
